## I'm open to work and [commissions](https://www.fiverr.com/tinnamchoi)! Have a look at my résumé / CV [here](https://gitlab.com/tinnamchoi/resumes/-/blob/master/Resume.pdf). 

### Contacts

[![Gmail](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto://tinnam.choi@gmail.com)
[![Microsoft Outlook](https://img.shields.io/badge/Microsoft_Outlook-0078D4?style=for-the-badge&logo=microsoft-outlook&logoColor=white)](mailto://tinnam.choi@student.adelaide.edu.au)
[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/tinnamchoi/)
[![Discord](https://img.shields.io/badge/Discord-5865F2?style=for-the-badge&logo=discord&logoColor=white)](https://discord.com/users/398756767510691852)
[![Instagram](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/tinnamchoi/)


### Languages

![Cantonese](https://img.shields.io/badge/Cantonese-FF0?style=for-the-badge&logoColor=white)
![English](https://img.shields.io/badge/English-FFF?style=for-the-badge&logoColor=white)
![Mandarin](https://img.shields.io/badge/Mandarin-F00?style=for-the-badge&logoColor=white)
![A1 German](https://img.shields.io/badge/A1%20German-FC0?style=for-the-badge&logoColor=white)

### Statistics

![Timothy's GitHub stats](https://github-readme-stats.vercel.app/api?username=tinnamchoi&count_private=true&show_icons=true&theme=github_dark&cache_seconds=14400&show_icons&hide_title=true)  
![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=tinnamchoi&theme=github_dark&cache_seconds=14400&show_icons&langs_count=10&layout=compact)
